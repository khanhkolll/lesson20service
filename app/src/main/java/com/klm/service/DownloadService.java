package com.klm.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class DownloadService extends Service implements MyTask.OnAsyncCallBack {
    private static final String TAG = DownloadService.class.getName();
    private static final int KEY_DOWNLOAD = 105;
    public static final String ACTION_SHOW_IMAGE = "ACTION_SHOW_IMAGE";
    public static final String ACTION_UPDATE_PROGRESS = "ACTION_UPDATE_PROGRESS";
    public static final String KEY_PROGRESS = "KEY_PROGRESS";
    private String savePath;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand...");
        download(intent);
        return START_NOT_STICKY;
    }

    private void download(Intent intent) {
        if (intent == null) {
            Toast.makeText(this, "Intent is null, could not download anymore", Toast.LENGTH_SHORT).show();
            stopSelf();
            return;
        }
        String link = intent.getStringExtra(MainActivity.KEY_LINK);

        new MyTask(this, KEY_DOWNLOAD).execute(link);

    }

    @Override
    public Object execTask(MyTask task, int key, Object... data) {
        String link = (String) data[0];
        if (link == null) return false;
        try {
            URLConnection url = new URL(link).openConnection();
            InputStream in = url.getInputStream();
            int totalSize = url.getContentLength();

            savePath = getExternalFilesDir(null).getPath() + "/" + new File(link).getName();
            File fileOut = new File(savePath);

            FileOutputStream out = new FileOutputStream(fileOut);

            byte[] buff = new byte[1024];
            int len = in.read(buff);
            int curentSize = 0;
            while (len > 0) {
                out.write(buff, 0, len);
                curentSize += len;
                int percent = (curentSize * 100 / totalSize);
                Log.i(TAG, "Download.." + percent + "%");
                task.updateTask(percent);
                len = in.read(buff);
            }
            in.close();
            out.close();
            return true;

        } catch (Exception e) {
            e.printStackTrace();

        }
        return false;
    }

//    https://en.wikipedia.org/wiki/Duck#/media/File:Bucephala-albeola-010.jpg

    @Override
    public void updateUI(int key, Object... values) {
        //send broadcast to act
        Intent intent = new Intent(ACTION_UPDATE_PROGRESS);
        intent.putExtra(KEY_PROGRESS, (int) values[0]);
        sendBroadcast(intent);
    }

    @Override
    public void taskComplete(int key, Object result) {
        if ((boolean) result) {
            Toast.makeText(this, "Download Complete", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Download fail", Toast.LENGTH_SHORT).show();
        }
        stopSelf();

        //send broadcast to act
        Intent intent = new Intent(ACTION_SHOW_IMAGE);
        intent.putExtra(MainActivity.KEY_LINK, savePath);
        sendBroadcast(intent);
    }
}
